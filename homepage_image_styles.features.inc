<?php

/**
 * @file
 * homepage_image_styles.features.inc
 */

/**
 * Implements hook_image_default_styles().
 */
function homepage_image_styles_image_default_styles() {
  $styles = array();

  // Exported image style: homepage_audience_panel.
  $styles['homepage_audience_panel'] = array(
    'label' => 'homepage_audience_panel',
    'effects' => array(
      2 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 500,
          'height' => 500,
        ),
        'weight' => 2,
      ),
    ),
  );

  // Exported image style: homepage_audience_panel_large.
  $styles['homepage_audience_panel_large'] = array(
    'label' => 'homepage_audience_panel_large',
    'effects' => array(
      4 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 500,
          'height' => 500,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: homepage_feature_stories.
  $styles['homepage_feature_stories'] = array(
    'label' => 'homepage_feature_stories',
    'effects' => array(
      13 => array(
        'name' => 'focal_point_scale_and_crop',
        'data' => array(
          'width' => 380,
          'height' => 320,
          'focal_point_advanced' => array(
            'shift_x' => '',
            'shift_y' => '',
          ),
        ),
        'weight' => 0,
      ),
    ),
  );

  // Exported image style: homepage_feature_stories_large.
  $styles['homepage_feature_stories_large'] = array(
    'label' => 'homepage_feature_stories_large',
    'effects' => array(
      15 => array(
        'name' => 'focal_point_scale_and_crop',
        'data' => array(
          'width' => 1000,
          'height' => 600,
          'focal_point_advanced' => array(
            'shift_x' => '',
            'shift_y' => '',
          ),
        ),
        'weight' => 0,
      ),
    ),
  );

  // Exported image style: homepage_feature_stories_medium.
  $styles['homepage_feature_stories_medium'] = array(
    'label' => 'homepage_feature_stories_medium',
    'effects' => array(
      17 => array(
        'name' => 'focal_point_scale_and_crop',
        'data' => array(
          'width' => 800,
          'height' => 400,
          'focal_point_advanced' => array(
            'shift_x' => '',
            'shift_y' => '',
          ),
        ),
        'weight' => 0,
      ),
    ),
  );

  // Exported image style: homepage_feature_stories_small.
  $styles['homepage_feature_stories_small'] = array(
    'label' => 'homepage_feature_stories_small',
    'effects' => array(
      19 => array(
        'name' => 'focal_point_scale_and_crop',
        'data' => array(
          'width' => 480,
          'height' => 480,
          'focal_point_advanced' => array(
            'shift_x' => '',
            'shift_y' => '',
          ),
        ),
        'weight' => 0,
      ),
    ),
  );

  // Exported image style: homepage_feature_stories_xl.
  $styles['homepage_feature_stories_xl'] = array(
    'label' => 'homepage_feature_stories_xl',
    'effects' => array(
      21 => array(
        'name' => 'focal_point_scale_and_crop',
        'data' => array(
          'width' => 1600,
          'height' => 553,
          'focal_point_advanced' => array(
            'shift_x' => '',
            'shift_y' => '',
          ),
        ),
        'weight' => 0,
      ),
    ),
  );

  // Exported image style: homepage_news.
  $styles['homepage_news'] = array(
    'effects' => array(
      9 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 55,
          'height' => 55,
        ),
        'weight' => 1,
      ),
    ),
    'label' => 'homepage_news',
  );

  // Exported image style: homepage_positioning_stories_big.
  $styles['homepage_positioning_stories_big'] = array(
    'effects' => array(
      2 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 130,
          'height' => 86,
        ),
        'weight' => 1,
      ),
    ),
    'label' => 'homepage_positioning_stories_big',
  );

  // Exported image style: homepage_positioning_stories_small.
  $styles['homepage_positioning_stories_small'] = array(
    'effects' => array(
      3 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 60,
          'height' => 40,
        ),
        'weight' => 1,
      ),
      7 => array(
        'name' => 'image_desaturate',
        'data' => array(),
        'weight' => 3,
      ),
    ),
    'label' => 'homepage_positioning_stories_small',
  );

  return $styles;
}
